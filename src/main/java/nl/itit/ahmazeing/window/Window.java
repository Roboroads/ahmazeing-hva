/*
 * Project ahmazeing.
 * Copyright AhMazeIng HvA Projectgroep (c) 2019.
 *
 * Class: nl.itit.ahmazeing.window.Window
 * Last editted on: 14-6-18 18:57
 */

package nl.itit.ahmazeing.window;

import nl.itit.ahmazeing.Generator.Maze;
import nl.itit.ahmazeing.Generator.Tile;
import nl.itit.ahmazeing.config.Args;

import javax.swing.*;
import java.awt.*;

public class Window extends JFrame {
    private static Window ourInstance = new Window();

    public static Window getInstance() {
        return ourInstance;
    }

    private final Color unused = new Color(255, 193, 166);
    private final Color wall = new Color(74, 161, 30);
    private final Color walkable = new Color(255, 255, 255);
    private final Color wallLock = new Color(42, 83, 17);
    private final Color walkableLock = new Color(141, 141, 141);

    private Maze maze;
    private DrawPanel drawPanel;
    private int squareWh;

    private Window() {
        super("Maze Generator");

        if(Args.getInstance().printSleep < 1) {
            //dont print - no print time added.
            return;
        }

        setVisible(true);
        drawPanel = new DrawPanel();
        add(drawPanel);
        setLocationRelativeTo(null);
        setResizable(true);
        setDefaultCloseOperation(Window.EXIT_ON_CLOSE);
    }


    class DrawPanel extends JPanel {

        private void doDrawing(Graphics g) {

            super.paintComponents(g);

            Graphics2D g2d = (Graphics2D) g.create();

            for (int y = 0; y < maze.getHeight(); y++) {
                for (int x = 0; x < maze.getWidth(); x++) {
                    Tile t = maze.get(x, y);
                    if (t.isUnused()) {
                        g2d.setColor(unused);
                    } else if (t.isWalkable()) {
                        if(t.isLocked()) {
                            g2d.setColor(walkableLock);
                        } else {
                            g2d.setColor(walkable);
                        }
                    } else {
                        if(t.isLocked()) {
                            g2d.setColor(wallLock);
                        } else {
                            g2d.setColor(wall);
                        }
                    }
                    g2d.fillRect(x * squareWh + 5, y * squareWh + 5, squareWh, squareWh);
                }
            }

            g2d.dispose();
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            doDrawing(g);
        }
    }


    public void setMaze(Maze maze) {
        this.maze = maze;
        this.squareWh = Args.getInstance().squareWidthHeight;
        this.setSize((maze.getWidth()+2) * squareWh + 25, (maze.getHeight()+5) * squareWh + 25);
    }

    public void reDraw() {
        drawPanel.revalidate();
    }
}
