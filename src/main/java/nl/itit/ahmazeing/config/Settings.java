/*
 * Project ahmazeing.
 * Copyright AhMazeIng HvA Projectgroep (c) 2019.
 *
 * Class: nl.itit.ahmazeing.config.Settings
 * Last editted on: 22-6-19 16:16
 */

package nl.itit.ahmazeing.config;

import java.util.UUID;

public class Settings {
    public static String MQ_CONSUMER = "AhMazeIng.calculateSquare";
    public static String MQ_PRODUCER = "AhMazeIng.calculatedSquare";
    public static String MQ_SERVER = "tcp://localhost:61616";
    private static String MQ_MSG_UUID;

    //For getting a run-uuid, so multiple generators can be ran on the same queue
    public static String uuid() {
        if(MQ_MSG_UUID == null) {
            MQ_MSG_UUID = UUID.randomUUID().toString();
        }
        return MQ_MSG_UUID;
    }
}
