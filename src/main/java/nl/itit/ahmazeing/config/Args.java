/*
 * Project ahmazeing.
 * Copyright AhMazeIng HvA Projectgroep (c) 2019.
 *
 * Class: nl.itit.ahmazeing.config.Args
 * Last editted on: 5-5-19 17:04
 */

package nl.itit.ahmazeing.config;

import com.beust.jcommander.Parameter;

import java.util.ArrayList;
import java.util.List;

public class Args {
    private static Args ourInstance = new Args();
    public static Args getInstance() { return ourInstance; }
    private Args() {}

    @Parameter
    public List<String> parameters = new ArrayList<String>();

    @Parameter(names = { "-w", "--width" }, description = "Sets width of the maze. Default: 10")
    public Integer width = 10;

    @Parameter(names = { "-h", "--height" }, description = "Sets height of the maze. Default: 10")
    public Integer height = 10;

    @Parameter(names = { "-t", "--threads" }, description = "Amount of threads. Default: local code count")
    public Integer threads = Runtime.getRuntime().availableProcessors();

    @Parameter(names = { "-p", "--printsleep" }, description = "If > 0 - prints all steps to a window so you can see the maze being built. Default: 0")
    public Integer printSleep = 0;

    @Parameter(names = { "-s", "--square" }, description = "Sets w/h of the squares being drawn (use -p to turn drawing on). Default: 30")
    public Integer squareWidthHeight = 30;

    @Parameter(names = { "-m", "--timing" }, description = "Enables timing.")
    public Boolean timing = false;

    @Parameter(names = { "-x", "--executions" }, description = "Executes multiple times.")
    public Integer executions = 1;

    @Parameter(names = { "-c", "--mmsize" }, description = "Sets the square the sent mini-maze will be.")
    public Integer miniMazeSize = 5;

    @Parameter(names = { "--help" }, help = true, description = "Displays params you can use")
    public boolean help;


}
