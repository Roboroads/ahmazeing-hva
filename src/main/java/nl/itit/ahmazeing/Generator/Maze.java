/*
 * Project ahmazeing.
 * Copyright AhMazeIng HvA Projectgroep (c) 2018.
 *
 * Class: nl.itit.ahmazeing.Generator.Maze
 * Last editted on: 6/7/18 4:26 PM
 */

package nl.itit.ahmazeing.Generator;

import nl.itit.ahmazeing.config.Args;
import nl.itit.ahmazeing.system.ThreadPool;
import nl.itit.ahmazeing.system.Timing;
import nl.itit.ahmazeing.window.Window;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Maze extends Vector<TileRow> {

    private Integer width;
    private Integer height;
    private Boolean preClaimed = false;
    private Direction lastRandomDirection;
    private Window window;

    public enum Direction {
        UP, DOWN, LEFT, RIGHT
    }

    public Maze(Integer width, Integer height) {
        super(height);
        this.width = width;
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public Integer getHeight() {
        return height;
    }

    public Integer getWidthEnd() {
        return width-1;
    }

    public Integer getHeightEnd() {
        return height-1;
    }

    public Direction getLastRandomDirection() {
        return lastRandomDirection;
    }

    public void preClaimSpace() {
        if(preClaimed) return;
        preClaimed = true;

        //Job that claims
        class ClaimJob implements Callable<Boolean> {
            private Maze maze;
            private int y;

            public ClaimJob(Maze maze, int y) {
                this.maze = maze;
                this.y = y;
            }

            public Boolean call() {
                // Make a tile row
                TileRow tileRow = new TileRow();

                for(int x = 0; x < maze.width; x++) {
                    // Claim tile space
                    tileRow.add(x, null);
                    // Add tiles to tile row
                    Tile tile = new Tile(x, y);

                    if(x == 0 || x == getWidthEnd() || y == 0 || y == getHeightEnd()) {
                        tile.setBorder();
                    }
                    //Adds some constraits when reacing a wall in generating a path.
                    if(x == 1 || x == 2 || x == getWidthEnd()-1 || x == getWidthEnd()-2) {
                        tile.addPathCreationIgnoreDirection(Direction.UP);
                    }
                    if(y == 1 || y == 2 || y == getHeightEnd()-1 || y == getHeightEnd()-2) {
                        tile.addPathCreationIgnoreDirection(Direction.LEFT);
                    }

                    tileRow.set(x, tile);
                }
                this.maze.set(this.y, tileRow);

                return true;
            }
        }

        ArrayList<Callable<Boolean>> tasks = new ArrayList<>();

        for(int y = 0; y < height; y++) {
            //Initting space
            this.add(y, null);
            //Adding task to the threadpool queue
            tasks.add(new ClaimJob(this, y));
        }

        //Creating a threadpool for creation task
        ThreadPool.devideTasksOverThreads(tasks);
    }


    public void simplePreClaimSpace() {
        if(preClaimed) return;
        preClaimed = true;

        for(int y = 0; y < height; y++) {
            //Initting space
            this.add(y, null);

            TileRow tileRow = new TileRow();

            for(int x = 0; x < this.width; x++) {
                // Claim tile space
                tileRow.add(x, null);
                // Add tiles to tile row
                Tile tile = new Tile(x, y);

                if(x == 0 || x == getWidthEnd() || y == 0 || y == getHeightEnd()) {
                    tile.setBorder();
                }

                tileRow.set(x, tile);
            }
            this.set(y, tileRow);
        }
    }


    public Tile get(int x, int y) {
        try {
            return this.get(y).get(x);
        } catch (Exception e) {
            return new Tile(0,0); //unwalkable tile
        }
    }

    public Tile relativeTile(Tile tile, Direction direction) {
        switch(direction) {
            case UP:
                return this.get(tile.x, tile.y-1);
            case DOWN:
                return this.get(tile.x, tile.y+1);
            case LEFT:
                return this.get(tile.x-1, tile.y);
            case RIGHT:
                return this.get(tile.x+1, tile.y);
        }
        System.out.println("NOPE");
        System.exit(1);
        return null;
    }

    public Tile relativeTile(Tile tile, ArrayList<Direction> excludedDirections) {
        Direction[] directions = {Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT};
        Direction randomDirection;
        Random random = new Random();

        do {
            randomDirection = directions[random.nextInt(directions.length)];
        } while(excludedDirections.contains(randomDirection));

        this.lastRandomDirection = randomDirection;
        return relativeTile(tile, randomDirection);
    }

    public Tile relativeTile(Tile tile) {
        Direction[] directions = {Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT};
        Direction randomDirection;
        Random random = new Random();

        randomDirection = directions[random.nextInt(directions.length)];

        return relativeTile(tile, randomDirection);
    }


    public void print() {
        Integer printSleep = Args.getInstance().printSleep;
        if(printSleep < 1) {
            //dont print - no print time added.
            return;
        }

        if(window == null) {
            window = Window.getInstance();
            window.setMaze(this);
        }

        window.repaint();

        try {
            Thread.sleep(printSleep);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
