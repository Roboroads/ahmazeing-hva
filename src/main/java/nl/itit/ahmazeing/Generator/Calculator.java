/*
 * Project ahmazeing.
 * Copyright AhMazeIng HvA Projectgroep (c) 2019.
 *
 * Class: nl.itit.ahmazeing.Generator.Calculator
 * Last editted on: 23-6-19 17:22
 */

package nl.itit.ahmazeing.Generator;

import nl.itit.ahmazeing.Generator.Maze;
import nl.itit.ahmazeing.Generator.Tile;
import nl.itit.ahmazeing.config.Args;
import nl.itit.ahmazeing.config.Settings;
import nl.itit.ahmazeing.system.CalculatorMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.util.ArrayList;

public class Calculator {

    private Maze maze;
    private CalculatorMessage calculatorMessage;
    private Session session;
    MessageProducer producer;

    public void run() throws JMSException {
//        Args.getInstance().printSleep = 2;
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Settings.MQ_SERVER);
        Connection connection = connectionFactory.createConnection();
        connection.start();

        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        MessageConsumer consumer = session.createConsumer(session.createQueue(Settings.MQ_CONSUMER));
        producer = session.createProducer(session.createQueue(Settings.MQ_PRODUCER));

        while(true) {
//            System.out.println("Waiting for task..");
            Message message = consumer.receive();

//            System.out.println("Received task, decoding..");

            try {
                ObjectMessage objectMessage = (ObjectMessage) message;
                calculatorMessage = (CalculatorMessage) objectMessage.getObject();

                // Initialize a new mini-maze
//                System.out.println("Init mini-maze..");
                maze = new Maze(calculatorMessage.width + 2, calculatorMessage.height + 2);
                maze.simplePreClaimSpace();

                int dataPointer = 0;
                for (int y = 0; y < calculatorMessage.height; y++) {
                    for (int x = 0; x < calculatorMessage.width; x++) {
                        Tile tile = maze.get(x+1,y+1);
                        int walkData = calculatorMessage.walkData[dataPointer++];
                        if(walkData == 1) {
                            tile.setWalkable();
                        } else {
                            tile.setUnWalkable();
                        }
                    }
                }

                //start walking bad path
//                System.out.println("Create bad path..");
                Tile currentTile = maze.get(calculatorMessage.width/2+1, calculatorMessage.height/2+1);
                Tile prevTile = null;

                while(true) {
                    ArrayList<Maze.Direction> excludeDirections = new ArrayList<>();

                    boolean moved = false;
                    for(Maze.Direction direction : Maze.Direction.values()) {
                        Tile nextTile = maze.relativeTile(currentTile, excludeDirections);
                        excludeDirections.add(maze.getLastRandomDirection());

                        if(prevTile != null && prevTile == nextTile) {
                            continue;
                        }
                        if(nextTile.isBorder()) {
                            break;
                        }

                        boolean goodTile = true;
                        for(Maze.Direction relDirection : Maze.Direction.values()) { //foreach direction -
                            Tile relTile = maze.relativeTile(nextTile, relDirection);
                            if(currentTile == relTile) {
                                continue;
                            }

                            if(relTile.isWalkable()) {
                                goodTile = false;
                                break;
                            }

                        }

                        if(!goodTile) {
                            continue;
                        }

                        nextTile.setWalkable();
                        prevTile = currentTile;
                        currentTile = nextTile;
                        moved = true;
                        break;
                    }

                    if(!moved) break;
                }

                maze.print();

                sendBack();

            } catch (Exception e) {
                e.printStackTrace();
//                System.out.println("ERROR");
            }
        }
    }

    private void sendBack() throws JMSException {

        int dataPointer = 0;
        for (int y = 0; y < calculatorMessage.height; y++) {
            for (int x = 0; x < calculatorMessage.width; x++) {
                calculatorMessage.walkData[dataPointer++] = (maze.get(x+1,y+1).isWalkable() ? 1 : 0);
            }
        }

//        System.out.println("Sending back bad path..");
        System.out.print(".");

        producer.send(session.createObjectMessage(calculatorMessage));
    }
}
