/*
 * Project ahmazeing.
 * Copyright AhMazeIng HvA Projectgroep (c) 2018.
 *
 * Class: nl.itit.ahmazeing.Generator.Tile
 * Last editted on: 6/7/18 4:26 PM
 */

package nl.itit.ahmazeing.Generator;

import java.util.ArrayList;

public class Tile {

    private Boolean walkable = false;
    private Boolean used = false;
    private Boolean border = false;
    private Boolean usedInSubPath = false;
    private Boolean locked = false;
    public final int x;
    public final int y;

    private ArrayList<Maze.Direction> pathCreationIgnoreDirections = new ArrayList<>();


    public Tile(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Boolean isWalkable() {
        if (!used) return false;  //Against nullpointer
        return walkable;
    }

    public Boolean isUnused() {
        return !used;
    }

    public Boolean isUsed() {
        return used;
    }

    public Boolean isUsedInSubPath() {
        return usedInSubPath;
    }

    public void setUsedInSubPath(Boolean usedInSubPath) {
        this.usedInSubPath = usedInSubPath;
    }

    public void setWalkable() {
        this.walkable = true;
        this.used = true;
    }

    public void setUnWalkable() {
        this.walkable = false;
        this.used = true;
    }

    public void setBorder() {
        this.setUnWalkable();
        this.border = true;
    }

    public void reset() {
        this.walkable = false;
        this.used = false;
    }

    @Override
    public String toString() {
        return "TILE: (" + x + ", " + y + ")";
    }

    public Boolean isBorder() {
        return border;
    }


    public ArrayList<Maze.Direction> getPathCreationIgnoreDirections() {
        return pathCreationIgnoreDirections;
    }

    public void addPathCreationIgnoreDirection(Maze.Direction direction) {
        if (!this.pathCreationIgnoreDirections.contains(direction)) {
            this.pathCreationIgnoreDirections.add(direction);
        }
    }

    public void resetPathCreationIgnoreDirection() {
        this.pathCreationIgnoreDirections = new ArrayList<Maze.Direction>();
    }

    public synchronized void lock() throws Exception {
        if (this.locked) throw new Exception("Could not acquire lock");
        this.locked = true;
    }

    public synchronized void unlock() {
        this.locked = false;
    }

    public synchronized Boolean isLocked() {
        return this.locked;
    }

}
