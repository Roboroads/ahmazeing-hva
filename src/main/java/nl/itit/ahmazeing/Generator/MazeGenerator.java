/*
 * Project ahmazeing.
 * Copyright AhMazeIng HvA Projectgroep (c) 2018.
 *
 * Class: nl.itit.ahmazeing.Generator.MazeGenerator
 * Last editted on: 6/7/18 4:26 PM
 */

package nl.itit.ahmazeing.Generator;

import nl.itit.ahmazeing.config.Args;
import nl.itit.ahmazeing.config.Settings;
import nl.itit.ahmazeing.system.CalculatorMessage;
import nl.itit.ahmazeing.system.ThreadPool;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Callable;

public class MazeGenerator {

    private Maze maze;
    private ArrayList<Tile> path = new ArrayList<>();
    private final ArrayList<Tile> walkTiles = new ArrayList<>();
    private final ArrayList<Tile> walkTilesRequeueException = new ArrayList<>();
    private final Maze.Direction[] allDirections = {
            Maze.Direction.UP,
            Maze.Direction.DOWN,
            Maze.Direction.LEFT,
            Maze.Direction.RIGHT
    };

    public MazeGenerator(Integer width, Integer height) {
        //Making width&height of the maze - adding 2 for borders
        this.maze = new Maze(width + 2, height + 2);

        if(!Args.getInstance().timing) System.out.print("Claiming vector space..");
        this.maze.preClaimSpace();
        if(!Args.getInstance().timing) System.out.println(" done!");

    }

    public Maze generate() {
        if(!Args.getInstance().timing) System.out.print("Creating main path..");
        this.generatePath();
        if(!Args.getInstance().timing) System.out.println(" done!");
        if(!Args.getInstance().timing) System.out.print("Walling off remaining unused spaced..");
        this.wallOffPath();
        if(!Args.getInstance().timing) System.out.println(" done!");
        if(!Args.getInstance().timing) System.out.print("Creating random new paths connecting to other paths..");
        try {
            this.createRandomDeadPaths();
        } catch (JMSException e) {
            e.printStackTrace();
        }
        if(!Args.getInstance().timing) System.out.println(" done!");

        if(!Args.getInstance().timing) System.out.println("Maze generated!");

        return maze;
    }

    private void generatePath() {
        // First: creating a path
        Tile currentTile = maze.get(1, 1); //0,0 = border
        Tile endTile = maze.get(maze.getWidthEnd() - 1, maze.getHeightEnd() - 1);
        currentTile.setWalkable();
        endTile.setWalkable();

        //entrance/exit
        Tile entranceTile = maze.get(1, 0);
        entranceTile.setWalkable();
        Tile exitTile = maze.get(maze.getWidthEnd() - 1, maze.getHeightEnd());
        exitTile.setWalkable();

        currentTile.addPathCreationIgnoreDirection(Maze.Direction.UP);
        currentTile.addPathCreationIgnoreDirection(Maze.Direction.LEFT);


        path.add(entranceTile); //first tile of path
        path.add(currentTile); // second tile of path


        while (currentTile != endTile) {
//            maze.print();

            //Moving back when dead end
            if (currentTile.getPathCreationIgnoreDirections().size() >= 4) {
                currentTile.reset();
                path.remove(path.size() - 1); //remove the bad tile
                currentTile = path.get(path.size() - 1); //set current tile one back
                continue;
            }

            Tile checkTile = maze.relativeTile(currentTile, currentTile.getPathCreationIgnoreDirections());
            Maze.Direction checkTileDirectionFromCurrent = maze.getLastRandomDirection();
            currentTile.addPathCreationIgnoreDirection(checkTileDirectionFromCurrent); // If we come back to this tile, don't go in this direction anymore since it lead to a dead end.

            if(checkTile == endTile) {
                //Done!
                currentTile = checkTile;
                checkTile.setWalkable();
                path.add(checkTile);
                break;
            }

            //Is this tile good?
            Boolean goodTile = true;
            if(checkTile.isBorder()) goodTile = false; //Cannot create path on a border.
            if(checkTile.isUsed()) goodTile = false; //Cannot create path on already in use tile.

            //Check surrounding tiles
            if(goodTile) {
                for(Maze.Direction direction : allDirections) {
                    Tile adjacentTile = maze.relativeTile(checkTile, direction);

                    if(adjacentTile == currentTile) continue; // This is where we came from so no need for checking.

                    //If an ajecent tile is already used - then we create a loop in the path - so bad tile.
                    if(adjacentTile.isUsed() && !adjacentTile.isBorder() && adjacentTile != endTile) {
                        goodTile = false;
                        break;
                    }
                }
            }

            //This tile is ok for making a path - adding it to the end of the known tiles that form the path (in order!) and make the new tile current tile.
            if(goodTile) {
                currentTile = checkTile;
                checkTile.setWalkable();
                path.add(checkTile);
            }
        }

        path.add(exitTile); //And the last tile of path.
        walkTiles.addAll(path);
        walkTiles.remove(entranceTile);
        walkTiles.remove(exitTile);
        walkTilesRequeueException.addAll(walkTiles);
    }

    private void wallOffPath() {
        //Job that walls off
        class WallingJob implements Callable<Boolean> {
            private Maze maze;
            private int y;

            public WallingJob(Maze maze, int y) {
                this.maze = maze;
                this.y = y;
            }

            public Boolean call() {
                TileRow tileRow = this.maze.get(y);
                for (Tile tile : tileRow) {
                    tile.resetPathCreationIgnoreDirection();

                    if (tile.isBorder() || !path.contains(tile)) {
                        tile.setUnWalkable();
                    }
                }

                maze.print();
                return true;
            }
        }

        ArrayList<Callable<Boolean>> tasks = new ArrayList<>();

        for (int y = 0; y < maze.getHeight(); y++) {
            //Adding task to the threadpool queue
            tasks.add(new WallingJob(maze, y));
        }

        //Creating a threadpool for walling task
        ThreadPool.devideTasksOverThreads(tasks);
    }

    private void createRandomDeadPaths () throws JMSException {
        //Job that creates random paths

        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(Settings.MQ_SERVER);
        Connection connection = connectionFactory.createConnection();
        connection.start();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        class deadPathJobProducer implements Callable<Boolean> {
            Random random = new Random();
            MessageProducer toCalculator;
            Session session;

            public deadPathJobProducer(Session session) throws JMSException {
                this.session = session;
                this.toCalculator = this.session.createProducer(session.createQueue(Settings.MQ_CONSUMER));
            }

            public Boolean call() {
                while(true) {
                    ArrayList<Tile> acquiredLocks = new ArrayList<>();
                    if(walkTiles.size() < 2) break;

                    try {
                        int randomIndex = random.nextInt(walkTiles.size() - 1);
                        Tile centerTile = walkTiles.get(randomIndex);
                        centerTile.lock(); //throws exception when locked - desired behaviour
                        acquiredLocks.add(centerTile);

                        int leftTop = Args.getInstance().miniMazeSize / 2;
                        CalculatorMessage calculatorMessage = new CalculatorMessage();
                        calculatorMessage.width = Args.getInstance().miniMazeSize;
                        calculatorMessage.height = Args.getInstance().miniMazeSize;
                        calculatorMessage.startPosX = centerTile.x;
                        calculatorMessage.startPosY = centerTile.y;
                        calculatorMessage.topLeftX = centerTile.x - leftTop;
                        calculatorMessage.topLeftY = centerTile.y - leftTop;
                        calculatorMessage.walkData = new int[(calculatorMessage.width * calculatorMessage.height)];

                        int dataPointer = 0;
                        for (int y = calculatorMessage.topLeftY; y < (calculatorMessage.topLeftY + calculatorMessage.height); y++) {
                            for (int x = calculatorMessage.topLeftX; x < (calculatorMessage.topLeftX + calculatorMessage.width); x++) {

                                if(x <= 0 || y <= 0 || x >= maze.getWidthEnd() || y >= maze.getHeightEnd()) {
                                    //ignore. out of bounds
                                    calculatorMessage.walkData[dataPointer++] = 0;
                                    continue;
                                }

                                Tile tile = maze.get(x, y);
                                if(tile != centerTile) {
                                    tile.lock(); //throws exception when locked - desired behaviour
                                    acquiredLocks.add(tile);
                                }

                                calculatorMessage.walkData[dataPointer++] = (tile.isWalkable() ? 1 : 0);
                            }
                        }

                        calculatorMessage.uuid = Settings.uuid();

                        toCalculator.send(session.createObjectMessage(calculatorMessage));
                        maze.print();

                    } catch (Exception ex) {
//                        System.out.println("Could not get locks");
                        for(Tile t : acquiredLocks) t.unlock();
                    }

                }

                return true;
            }
        }

        //Job that creates random paths
        class deadPathJobConsumer implements Callable<Boolean> {
            MessageConsumer fromCalculator;
            Session session;

            public deadPathJobConsumer(Session session) throws JMSException {
                this.session = session;
                this.fromCalculator = session.createConsumer(session.createQueue(Settings.MQ_PRODUCER));
            }

            public Boolean call() {
                while(true) {
                    try {
                        Message message = fromCalculator.receive(1000);
                        ObjectMessage objectMessage = (ObjectMessage) message;
                        CalculatorMessage calculatorMessage = (CalculatorMessage) objectMessage.getObject();
                        if(!calculatorMessage.uuid.equals(Settings.uuid())) continue; //From a previous run - discard.

                        Tile centerTile = maze.get(calculatorMessage.startPosX, calculatorMessage.startPosY);
                        walkTiles.remove(centerTile);

                        int dataPointer = 0;
                        for (int y = calculatorMessage.topLeftY; y < (calculatorMessage.topLeftY + calculatorMessage.height); y++) {
                            for (int x = calculatorMessage.topLeftX; x < (calculatorMessage.topLeftX + calculatorMessage.width); x++) {
                                if(x <= 0 || y <= 0 || x >= maze.getWidthEnd() || y >= maze.getHeightEnd()) {
                                    //ignore. out of bounds
                                    dataPointer++;
                                    continue;
                                }

                                Tile tile = maze.get(x, y);
                                if(calculatorMessage.walkData[dataPointer++] == 1) {
                                    tile.setWalkable();
                                    synchronized (walkTilesRequeueException) {
                                        if(!walkTilesRequeueException.contains(tile)) {
                                            walkTilesRequeueException.add(tile);
                                            walkTiles.add(tile);
                                        }
                                    }
                                }

                                tile.unlock(); // can be picked up again.
                            }
                        }

                        maze.print();
                    } catch (Exception ex) {
                        // When the queue has no messages for a second, you can check if you are done.
                        if(walkTiles.size() < 2) break;
                    }
                }

                return true;
            }
        }

        ArrayList<Callable<Boolean>> tasks = new ArrayList<>();

        for(int tp = Args.getInstance().threads/4; tp > 0; tp--) {
            tasks.add(new deadPathJobProducer(session));
        }
        for(int tp = Args.getInstance().threads; tp > 0; tp--) {
            tasks.add(new deadPathJobConsumer(session));
        }


        ThreadPool.devideTasksOverThreads(tasks, tasks.size());
    }
}
