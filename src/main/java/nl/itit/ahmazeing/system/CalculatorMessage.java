/*
 * Project ahmazeing.
 * Copyright AhMazeIng HvA Projectgroep (c) 2019.
 *
 * Class: nl.itit.ahmazeing.system.CalculatorMessage
 * Last editted on: 22-6-19 16:29
 */

package nl.itit.ahmazeing.system;

import java.io.Serializable;

public class CalculatorMessage implements Serializable {
    public String uuid;
    public int width = 5;
    public int height = 5;
    public int startPosX = 1;
    public int startPosY = 1;
    public int topLeftX = 1; //for reporting back
    public int topLeftY = 1;
    public int[] walkData;
}
