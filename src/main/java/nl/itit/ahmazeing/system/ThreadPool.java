/*
 * Project ahmazeing.
 * Copyright AhMazeIng HvA Projectgroep (c) 2019.
 *
 * Class: nl.itit.ahmazeing.system.ThreadPool
 * Last editted on: 6/15/19 7:35 PM
 */

package nl.itit.ahmazeing.system;

import nl.itit.ahmazeing.config.Args;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ThreadPool {
    public static void devideTasksOverThreads(ArrayList<Callable<Boolean>> tasks) {
        ExecutorService executorService = Executors.newFixedThreadPool(Args.getInstance().threads);

        run(tasks, executorService);
    }

    public static void devideTasksOverThreads(ArrayList<Callable<Boolean>> tasks, int threads) {
        ExecutorService executorService = Executors.newFixedThreadPool(threads);

        run(tasks, executorService);
    }

    private static void run(ArrayList<Callable<Boolean>> tasks, ExecutorService executorService) {
        try {
            Timing.startParallel();
            executorService.invokeAll(tasks);
            executorService.shutdown();
            executorService.awaitTermination(7, TimeUnit.DAYS); //Block until tasks finish
            Timing.stopParallel();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Error - executorService got interrupted.");
            System.exit(1);
        }
    }
}
