/*
 * Project ahmazeing.
 * Copyright AhMazeIng HvA Projectgroep (c) 2019.
 *
 * Class: nl.itit.ahmazeing.system.Timing
 * Last editted on: 6/15/19 11:05 PM
 */

package nl.itit.ahmazeing.system;

import nl.itit.ahmazeing.config.Args;

public class Timing {

    private static long startTime;
    private static long parallelStartTime;
    private static long parallelTime = 0;


    public static void start() {
        if(Args.getInstance().timing) startTime = System.currentTimeMillis();
        parallelTime = 0;
    }

    public static void startParallel() {
        if(Args.getInstance().timing) parallelStartTime = System.currentTimeMillis();
    }
    public static void stopParallel() {
        if(Args.getInstance().timing) parallelTime += System.currentTimeMillis() - parallelStartTime;
    }

    public static void print(String id) {
        if(Args.getInstance().timing) {
            long elapsedTime = System.currentTimeMillis() - startTime;
            System.out.println(id+" -> " + elapsedTime + " - SERIAL: " + (elapsedTime-parallelTime) + ", PARALLEL: " + parallelTime);
        }
    }
}
