/*
 * Project ahmazeing.
 * Copyright AhMazeIng HvA Projectgroep (c) 2019.
 *
 * Class: nl.itit.ahmazeing.Main
 * Last editted on: 5-5-19 17:04
 */

package nl.itit.ahmazeing;

import com.beust.jcommander.JCommander;
import nl.itit.ahmazeing.config.Args;
import nl.itit.ahmazeing.Generator.Maze;
import nl.itit.ahmazeing.Generator.MazeGenerator;
import nl.itit.ahmazeing.system.Timing;

public class Main {

    public static void main(String[] argv) {
        System.out.println("AhMazeIng by AhMazeIng HvA Projectgroep");

        Args args = Args.getInstance();
        JCommander.newBuilder()
                .addObject(args)
                .build()
                .parse(argv);

        if(args.help) {
            System.out.println("Params (command [default]):");
            System.out.println("  -w or --width [10]");
            System.out.println("    Sets width of the maze.");
            System.out.println();
            System.out.println("  -h or --height [10]");
            System.out.println("    Sets height of the maze.");
            System.out.println();
            System.out.println("  -t or --threads ["+Runtime.getRuntime().availableProcessors()+"]");
            System.out.println("    Amount of threads in threadpool for multithreaded execution of certain tasks.");
            System.out.println("    (Default: local core count)");
            System.out.println();
            System.out.println("  -p or --printsleep [0]");
            System.out.println("    Thread sleep time after each print.");
            System.out.println("    This controls how fast you will see the maze being built step-by-step.");
            System.out.println("    Set speel time will occur after EACH step.");
            System.out.println("    0 turns off printing completely.");
            System.out.println();
            System.out.println("  -s or --square [30]");
            System.out.println("    Sets w/h of the squares being drawn (use -p to turn drawing on).");
            System.out.println();
            System.out.println("  -m or --timing");
            System.out.println("    Turns on timing.");
            System.out.println();
            System.out.println("  -x or --executions [1]");
            System.out.println("    Sets amount of executions");
            System.out.println();
            System.out.println("  --help");
            System.out.println("    Displays params you can use (You are using it right now.. so.. yea..)");
            System.out.println();
            System.exit(0);
            return;
        }

        if(args.width < 2 || args.height < 2) {
            System.out.println("Can only create mazes that are at least 2x2.. But you ain't an idiot, are you?");
            System.exit(1);
            return;
        }

        System.out.println("Using:");
        System.out.println("  - Width: " + args.width + (args.width<4 ? " (You really want a crap maze do you?)" : ""));
        System.out.println("  - Height: " + args.height + (args.height<4 ? " (You really want a crap maze do you?)" : ""));
        System.out.println("  - Thread pool: " + args.threads);
        System.out.println("  - Print-Sleep Time: " + args.printSleep);
        if(args.printSleep > 0) System.out.println("  - SquateWH: " + args.squareWidthHeight);
        System.out.println();
        System.out.println("Starting generator..");

        for(Integer execution = 1; execution <= args.executions; execution++) {
            Timing.start();

            MazeGenerator mazeGenerator = new MazeGenerator(args.width, args.height);
            Maze maze = mazeGenerator.generate();
            maze.print();

            Timing.print(execution.toString());
        }

    }
}
