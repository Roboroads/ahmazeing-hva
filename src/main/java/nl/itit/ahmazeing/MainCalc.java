/*
 * Project ahmazeing.
 * Copyright AhMazeIng HvA Projectgroep (c) 2019.
 *
 * Class: nl.itit.ahmazeing.MainCalc
 * Last editted on: 23-6-19 17:44
 */

package nl.itit.ahmazeing;

import nl.itit.ahmazeing.Generator.Calculator;

public class MainCalc {

    public static void main(String[] argv) throws Exception {
//        BasicConfigurator.configure();
        Calculator calculator = new Calculator();
        calculator.run();
    }

}
