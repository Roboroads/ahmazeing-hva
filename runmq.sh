#!/usr/bin/env bash

docker run --name "mq" -it --rm -p 61616:61616 -p 8161:8161 -v C:/dstor/mq/conf:/opt/activemq/conf -v C:/dstor/mq/data:/opt/activemq/data  rmohr/activemq

chown activemq:activemq /mnt/conf
chown activemq:activemq /mnt/data
cp -a /opt/activemq/conf/* /mnt/conf/
cp -a /opt/activemq/data/* /mnt/data/




docker run --name "mq" -it --rm -p 1883:1883 -p 5672:5672 -p 8161:8161 -p 61613:61613 -p 61614:61614 -p 61616:61616 webcenter/activemq:latest